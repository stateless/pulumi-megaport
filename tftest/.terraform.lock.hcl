# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/megaport/megaport" {
  version = "0.2.5"
  hashes = [
    "h1:TWPQiYbE4/Tsqyq/to+KQ7mZsLSdO/QgTAggE3OYq5o=",
    "zh:0291678f996a24e946614234f6365e72fe1e6108cc9cc7f03a43ec0b2ba5dbed",
    "zh:03abfe8504d0608a390cde7f8947d865290b4f9f1e5b8382261276a6be3a510a",
    "zh:0bb002bb90bb70442ee5dc13841d95cdfeb61b8bd4cc293c260daafff6a6a958",
    "zh:17b6e5e7a211242bfe595ea43da060fa8cae305fc0c02ab47bf1937c968a7c1d",
    "zh:18f9519ae34c7baf8995ca1576016eb02f5fcf19741491edf88813a7eb32fe52",
    "zh:1faa56b821e95c84fc168197dc2b0236fedb8876be16b1393320ad56f8fd2549",
    "zh:467a63a143a498ca614a4c89964582d8fd429fd300e69475e3d55c5c728964ae",
    "zh:50efdaf4ba4ef0dc71efc54fb919e6f2d9879a9e72a8507154c9686bcfbae14f",
    "zh:5f7a03418196659fe8b27b4da4bd8dfa3a985a33907f1bdc5e2a819abaf1a9fa",
    "zh:6fb6d374114c17c0629bc3c9c066a36a77bc5c5ef7e7e3aed5c251d54ac4cc82",
    "zh:74db1f8e0fd1b46da248333904ddeed00fb3fd16c5103bfc088b106bfe1ebbba",
    "zh:a61a9e0fdda0677ce10e99e6e5615e47c1792073ce13ad00adf9f3971a60e1bb",
    "zh:d6ca4baca3f7925cb4f03de8264e25862021a11da7ee6a38e49eb2ae52ebd7cc",
    "zh:e031dc38b25613c8e039cfb12c1af5ea46de7a3674c54220d48057d52176dbf4",
  ]
}
